var _shootTheMoon = _shootTheMoon ? _shootTheMoon : {};
(function($) {
	$.extend(_shootTheMoon, {
		pageReadyClass: function() {
			var self = this;

			self.settings.$html.addClass('page-ready');
		},
		pageLoadedClass: function() {
			var self = this;

			self.settings.$html.addClass('page-loaded');
		}
	});
	$.subscribe('pageReady', function() {
		_shootTheMoon.pageReadyClass();
	});
	$.subscribe('pageLoaded', function() {
		_shootTheMoon.pageLoadedClass();
	});
}(jQuery));
