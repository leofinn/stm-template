var _shootTheMoon = _shootTheMoon ? _shootTheMoon : {};
(function ($) {
	$.extend(_shootTheMoon, {
		$selector: null,
		videosResponsive: function ($object) {
			if (!$object) {
				$object = $('.media-video');
			}
			$object.fitVids({
				customSelector: 'iframe[src^="http://' + window.location.host + '"], iframe[src^="http://fast.wistia.net"], iframe[src^="http://cdnapi.kaltura.com"]'
			});
		}
	});
	$.subscribe('pageReady', function () {
		_shootTheMoon.videosResponsive();
	});
}(jQuery));