var _shootTheMoon = _shootTheMoon ? _shootTheMoon : {};
(function($) {
	$.extend(_shootTheMoon, {
		stmActionMenu: function() {
			var self = this,
				$demoActions = $('.demo-actions'),
				menuIn = false,
				delayA = null,
				delayB = null,
				wait = false;

			if (!$demoActions.length) {
				return;
			}

			$('.action-demo-actions').on('click.stmActionMenu', function(event) {
				event.preventDefault();

				if (wait) {
					return;
				}

				wait = true;

				if (menuIn) {
					_shootTheMoon.settings.$html.removeClass('demo-actions-show');

					delayA = setTimeout(function() {
						_shootTheMoon.settings.$html.removeClass('demo-actions-out');
						menuIn = false;
						wait = false;
						clearTimeout(delayA);
					}, 250);

				} else {
					_shootTheMoon.settings.$html.addClass('demo-actions-out');

					delayB = setTimeout(function() {
						_shootTheMoon.settings.$html.addClass('demo-actions-show');
						menuIn = true;
						wait = false;
						clearTimeout(delayB);
					}, 250);
				}
			});
		}
	});
	$.subscribe('pageReady', function() {
		_shootTheMoon.stmActionMenu();
	});
}(jQuery));
