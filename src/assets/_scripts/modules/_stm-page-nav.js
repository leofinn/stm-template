var _shootTheMoon = _shootTheMoon ? _shootTheMoon : {};
(function($) {
	$.extend(_shootTheMoon, {
		stmPageNav: function() {
			var self = this,
				$pageNav = $('.stm-page-nav'),
				navOpen = false;

			if (!$pageNav.length) {
				return;
			}

			// create open nav btn
			var $btn = $('<button />', {
				'type': 'button',
				'class': 'action-stm-page-nav stm-page-nav-btn btn-no-style'
			}).text('menu');
			var $demoActions = $('.demo-links');
			if ($demoActions && $demoActions.length > 0) {
				$demoActions.append($btn);
			}

			// open nav on btn click
			$btn.on('click.stmPageNav', function(event) {
				_shootTheMoon.settings.$html.toggleClass('stm-page-nav-show');

				if (navOpen) {
					navOpen = false;
				} else {
					navOpen = true;
				}
			});

			// close nav on close button click
			$('.action-stm-page-nav-close').on('click.stmPageNav', function(event) {
				event.preventDefault();
				_shootTheMoon.settings.$html.removeClass('stm-page-nav-show');
				navOpen = false;
			});

			// close nav on body click
			_shootTheMoon.settings.$htmlbody.on('click.stmPageNav', function(event) {
				var $clickElement = $(event.target),
					$actionBtn = $clickElement.closest('.action-stm-page-nav'),
					$pageNav = $clickElement.closest('.stm-page-nav');

				if (($actionBtn && $actionBtn.length > 0) || ($pageNav && $pageNav.length > 0)) {
					return;
				}

				if (navOpen) {
					_shootTheMoon.settings.$html.removeClass('stm-page-nav-show');
					navOpen = false;
				} else {
					return;
				}
			});
		}
	});
	$.subscribe('pageReady', function() {
		_shootTheMoon.stmPageNav();
	});
}(jQuery));
